#!/usr/bin/env python
# -*- coding: latin-1-*-

"""
    Copyright (C) 2018  Michael Anderegg <michael@anderegg.be>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import time
import tkinter as tk
from threading import Thread

# my very own classes
from firefinder.ff_miscellaneous import create_image, get_local_ip_address
from firefinder.ff_top import TopBar

""" Path's """
# ffLogo = 'pic/Logo.png'  # Firefighter Logo


class ScreenSplash(tk.Frame):
    def __init__(self, parent, controller):
        super(ScreenSplash, self).__init__(parent)
        
        self.parent = parent
        self.controller = controller
        
        self.logo_path_and_file = ''
        self.show_local_ip_address = False
        
        # store size of the parent frame        
        self.controller.update()
        self.screenWidth = self.controller.winfo_width()
        self.screenHeight = self.controller.winfo_height()
        self.topBarHeight = 30
        self.emblemBarHeight = self.screenHeight - self.topBarHeight

        # create some instance for the widget
        self.topHeader = TopBar(self, height=self.topBarHeight)
        self.emblemBar = tk.Canvas(self)
        self.picture = tk.Label(self.emblemBar)
        self.emptyBar = tk.Canvas(self)
        self.image = ''
        
        self.IpAddressLabel = None
        
        # create widget
        self.create_widget()
        
        # show as soon as mainloop is idle
        self.after_idle(self.show)
    
    # ----------------------------------------------------------------------
    def create_widget(self):
        
        """ Create a TopBar object """
        self.topHeader.configure(showLogo=False)
        self.topHeader.pack(fill='both')
        
        ''' Create a canvas to hold the top emblem '''
        self.emblemBar.config(width=self.screenWidth,
                              height=self.emblemBarHeight,
                              background='black',
                              highlightthickness=0)
        
        self.picture.config(bd=0,
                            background='black',
                            foreground='black')
        
        ''' Create a canvas to hold a failure image '''
        self.emptyBar.config(width=self.screenWidth,
                             height=self.emblemBarHeight,
                             background='black',
                             highlightthickness=0)
        
        self.emptyBar.create_line(0, 0, self.screenWidth, self.screenHeight, fill='red', width=5)
        self.emptyBar.create_line(self.screenWidth, 0, 0, self.screenHeight, fill='red', width=5)
        self.IpAddressLabel = self.emptyBar.create_text(self.screenWidth - 40,
                                                        self.screenHeight - 80,
                                                        text="",
                                                        font=("Arial", 20),
                                                        fill="Red",
                                                        anchor='se',
                                                        justify='right')
    
    # ----------------------------------------------------------------------
    def show(self):
    
        # Create a logo with the firefighter Emblem
        if os.path.isfile(self.logo_path_and_file):
            # create screen for object
            self.image = create_image(self,
                                      path=self.logo_path_and_file,
                                      width=self.screenWidth - 20,
                                      height=self.emblemBarHeight - 20)

            self.picture['image'] = self.image
            self.emptyBar.pack_forget()
            self.emblemBar.pack(fill='both')
            top_paddy = int((self.emblemBarHeight / 2) - (self.image.height() / 2))
            self.picture.pack(fill='both', ipady=top_paddy)
        else:
            if self.show_local_ip_address:
                address_list = get_local_ip_address()
                address_list = ['{0}\n'.format(element) for element in address_list]
                address_string = ''.join(address_list)
                address = "IP-Addresse:\n==========\n" + address_string
            else:
                address = ''
            self.emptyBar.itemconfig(self.IpAddressLabel, text=address)

            self.emblemBar.pack_forget()
            self.emptyBar.pack(fill='both')
    
    # ----------------------------------------------------------------------
    def configure(self, **kwargs):
        if len(kwargs) == 0:  # return a dict of the current configuration
            cfg = {'logoPathAndFile'    : self.logo_path_and_file,
                   'showLocalIpAddress' : self.show_local_ip_address}
            return cfg
        
        else:  # do a configure
            for key, value in list(kwargs.items()):
                if key == 'logoPathAndFile':
                    self.logo_path_and_file = value
                if key == 'showLocalIpAddress':
                    self.show_local_ip_address = value
                    self.show()
    
    # ----------------------------------------------------------------------
    def descent_screen(self):
        self.topHeader.descent_screen()
        pass
    
    # ----------------------------------------------------------------------
    def raise_screen(self):
        self.topHeader.raise_screen()
        pass


########################################################################
def test_screensplash():
    print("Start Test")
    time.sleep(1)
    
    screen.configure(showLocalIpAddress=True)
    time.sleep(5)
    
    screen.configure(showLocalIpAddress=False)
    time.sleep(1)


######################################################################## 
if __name__ == '__main__':
    root = tk.Tk()
    screenwidth = root.winfo_screenwidth()
    screenhigh = root.winfo_screenheight()
    
    if screenwidth > 1920:
        screenwidth = 1920
    
    if screenhigh > 1080:
        screenhigh = 1080
    
    root.geometry("%dx%d+0+0" % (screenwidth, screenhigh))
    
    container = tk.Frame(root)
    container.pack(side="top", fill="both", expand=True)
    container.grid_rowconfigure(0, weight=1)
    container.grid_columnconfigure(0, weight=1)
    
    screen = ScreenSplash(container, root)
    screen.grid(row=0, column=0, sticky="nsew")
    screen.tkraise()
    
    thread = Thread(target=test_screensplash, args=())
    thread.start()
    root.mainloop()

